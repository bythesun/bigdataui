#!/bin/sh

# wait for RabbitMQ server to start
sleep 10s

#cd ds_uis

# run Celery worker for our project myproject with Celery configuration stored in Celeryconf
celery worker -A ds_uis