from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.add, name='add'),
    url(r'^check/(?P<celery_async_result_id>[\w-]+)/$', views.check, name='check'),
]