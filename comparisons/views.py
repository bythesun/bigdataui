from django.http import HttpResponse
from . import tasks
from ds_uis.celery import app


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def add(request):
    result = tasks.add.delay(4, 4)
    return HttpResponse(result)


def check(request, celery_async_result_id):
    result = app.AsyncResult(celery_async_result_id)

    return HttpResponse(result.state)
