from __future__ import absolute_import, unicode_literals
from ds_uis.celery import app
import time


@app.task
def add(x, y):
    time.sleep(30)
    return x + y
